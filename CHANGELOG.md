# Changelog

## [v1.2.1-SNAPSHOT] - 2020-10-08

### Features

- removec jackson depenedency

## [v1.2.0] - 2019-09-16

### Features

- Feature #17265, oAuth2 service with capability to be deployed on a multi instance cluster

## [v1.1.0] - 2019-03-21

### Features

- oauth2 fully compatible reading client_id and client_secret from Basic authorization

## [v1.0.0] - 2017-01-31

First release


This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).