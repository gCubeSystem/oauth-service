package org.gcube.portal.oauth.cache;


import java.io.Serializable;

/**
 * @author Massimiliano Assante, National Research Council of Italy (CNR) - ISTI
 */
@SuppressWarnings("serial")
public class MemCachedBean implements Serializable {
	private String token;
	private String scope;
	private String errorMessage;
	private boolean success;
	
	public MemCachedBean(String errorMessage) {
		this.errorMessage = errorMessage;
		this.success = false;
	}
	
	public MemCachedBean(String token, String scope) {
		super();
		this.token = token;
		this.scope = scope;
		this.success = true;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public boolean isSuccess() {
		return success;
	}
	
}

